
		// needs to be global to be accessible after a function has run
		var lastButton;
		var prevResults = [];



		createButtons();


		// goes from here to addButton to createClick to addToInput
		function createButtons() {


				var numPad = document.getElementById("numPad");

				var topFuncs = document.getElementById("topFuncs");

				var sideFuncs = document.getElementById("sideFuncs");

				var arrows = document.getElementsByClassName("arrows");



				var topFuncList = ["AC", "SIN", "COS", "TAN", "2nd", "(", ")", "%", "π", "!", "√", "del"];

				var sideFuncList = ["DM", "^", "÷", "x", "–", "+", "="];


				var lastRow = [".", "-"];




				loop(topFuncs, topFuncList);

				loop(sideFuncs, sideFuncList);

				// loads number in the order on all calculators I've seen
				for (var i = 0, num = 0; i < 10; i++, num--) {

						num = (i === 9) ? 10 : (i % 3 === 0) ? (i + 3) : num;

						addButton(numPad, 10 - num);
				}

				loop(numPad, lastRow);




				for (var i = 0, len = arrows.length; i < len; i++) {
						createClick(arrows[i].id, arrows[i].id);
				}


				// sends to keyboard event to filter out invalid inputs
				addKeys(topFuncList.concat(sideFuncList).concat(lastRow));



				function loop(element, array) {

						for (var i = 0, len = array.length; i < len; i++) {

								addButton(element, array[i]);
						}
				}
		}







			function addButton(father, command) {

					var buttonId = "func-" + command;


					if (/DM/.test(command)) {

							var child  = document.createElement("a");
							child.setAttribute("href", "https://github.com/DovidM/js-calculator");
							child.setAttribute("target", "_blank");
							child.innerHTML = "<button> <strong> DM </strong> </button>";
					}

					else {
							var child  = document.createElement("button");
							child.innerHTML = command;
					}


					if (/^S|^C|^T/.test(command)) {
							child.setAttribute("class", "trig");
					}

					child.id = buttonId;
					father.appendChild(child);

					createClick(buttonId, command);
			}





			function createClick(element, command) {


					document.getElementById(element).addEventListener("click", function() {

							addToInput(command);

					});
			}


			function addKeys(allowed) {

					document.body.addEventListener("keypress", function(e) {

						var key = String.fromCharCode(e.which);



							switch (e.keyCode) {

								case 13:
										// enter key
										key = "=";
										break;

								case 37:
										return addToInput("left");

								case 38:
										return addToInput("up");

								case 39:
										return addToInput("right");

								case 40:
										return addToInput("down");
							}


							switch (key) {
								case "*":
										key = "x";
										break;

								// switch minus and negative (longer is minus)
								case "-":
										key = "–";
										break;

								case "–":
										key = "-";
										break;

								case "s":
										return addToInput("SIN");

								case "c":
										return addToInput("COS");

								case "t":
										return addToInput("TAN");

								case "S":
										return addToInput("ASIN");

								case "C":
										return addToInput("ACOS");

								case "T":
										return addToInput("ATAN");

								case "d":
										return del();
							}




							if (allowed.indexOf(key) !== -1 || /\d/.test(key)) {

									return addToInput(key);
							}

					});

			}




			function addToInput(command) {

					var output = document.getElementById("output");

					if (lastButton === "=") {
							output.value = "";
					}


					// SIN --> ASIN
					if (lastButton === "2nd" && /^S|^C|^T/.test(command)) {
							command = "A" + command;
					}

					// adds parenthesis to trig (Sin, Cos, Tan, and the arcs)
					// A needes .. to check if AC or an arc
					if (/^S|^C|^T|^A..|√/.test(command)) {
							command += "(";
					}

					if (lastButton === "2nd" || command === "2nd") {

							var classAction = (command === "2nd") ? "add" : "remove";

							changeClass(document.getElementsByClassName("trig"), classAction, "multiUse");
					}


					lastButton = command;


					// arrows
					if (/^u|^do/.test(command)) {

							return prevAns(command, output);
					}

					if (/^r|^l/.test(command)) {

							return moveCursor(command, output);
					}

					switch (command) {
						case "AC":
								return output.value = 0;

						case "=":
								return result(output, output.value);

						case "del":
								return del(output);
					}


					// blocks DM and 2nd from printing
				 	if (/^DM|^2nd/.test(command)) {
							return;
					}

					// anything but a decimal makes a 0 standing alone be replaced; the decimal gets added to it
					else if (output.value == 0 && command !== "." && output.value.length === 1) {
							return output.value = command;
					}


					output.value += command;
			}



			function prevAns(action, output) {


					var curAns = prevResults.indexOf(output.value);


					if (action === "up") {

							if (typeof(prevResults[curAns - 1]) === "undefined") {
									return output.value === 0;
							}

							// if already looking at a previous answer, go one back
							if (curAns !== -1) {
									return output.value = prevResults[curAns - 1];
							}

							return output.value = prevResults[prevResults.length - 1];
					}


					if (action === "down") {

							if (typeof(prevResults[curAns + 1]) === "undefined") {
									return output.value === 0;
							}

							return output.value = prevResults[curAns + 1];
					}

			}




			function del(output) {

					output.focus();

					// if cursor's at the beginning, assume user didn't do it (since automatiaclly goes there on focus) and delete last char
					var startInd = (output.selectionStart === 0 || output.selectionStart === output.value.length) ? output.value.length - 1 : output.selectionStart;


					var start = output.value.substring(0, startInd);

					var end = output.value.substring(startInd + 1);

					output.value = start + end;

					output.setSelectionRange(startInd, startInd);
			}



			function moveCursor(action, output) {

					var curPos = output.selectionStart;


					output.focus();

					switch (action) {
						case "right":
								return output.setSelectionRange(curPos + 1, curPos);

						case "left":
								return output.setSelectionRange(curPos - 1, curPos);
					}
			}



		function changeClass(permClass, action, classToChange) {

				var classes = document.getElementsByClassName("trig");

				// error checking
				if (action === "remove" && !classes[0].classList.contains(classToChange)) {

						return;
				}

				if (action === "add" && classes[0].classList.contains(classToChange)) {

						return;
				}




				for (var i = 0, len = classes.length; i < len; i++) {

						if (action === "add") {
								classes[i].classList.add(classToChange);
						}

						else {
								classes[i].classList.remove(classToChange);
						}
				}
		}




		function result(output, input) {

				// splits input based on where the operators are
				var result = input.split(/(\D)/);


				// gets the input boiled down to plain old numbers with nothing fancy
				replaceMisc(result);
				parenthMisc(result);



				if (result.length > 1) {
						arith(result);
				}

				result = result.join();

				output.value = result;

				prevResults.push(result);

		}




		function arith(outputArr) {

				var pemdas = ["^", "%", "x", "÷", "+", "–"];

				var res = 0;


				// checks for pemdas
				for (var i = 0, pemLen = pemdas.length; i < pemLen; i++) {

						// if a value of pemdas is found
						if (outputArr.indexOf(pemdas[i]) !== -1) {

								var pemInd = outputArr.indexOf(pemdas[i]);
								var nextPemInd = outputArr.indexOf(pemdas[i + 1]);

								var pem = pemdas[i];

								// whichever in PEMDAS that can come in either order, the one most left goes
								if (/x|\+/.test(pemdas[i]) && nextPemInd !== -1) {

										pem = (nextPemInd < pemInd) ? pemdas[i + 1] : pem;
								}

								break;
						}
				}


				// gets numbers on either side of operator
				var num1 = Number(outputArr[outputArr.indexOf(pem) - 1]);
				var num2 = Number(outputArr[outputArr.indexOf(pem) + 1]);



				switch (pem) {

					case "+":
							res = num1 + num2;
							break;

					case "–":
							res = num1 - num2;
							break;

					case "x":
							res = num1 * num2;
							break;

					case "÷":
							res = num1 / num2;
							break;

					case "^":
							res = Math.pow(num1, num2);
							break;

					case "%":
							res = num1 % num2;
							break;
				}


				outputArr.splice(outputArr.indexOf(pem) - 1, 3, res);


				if (outputArr.length > 1) {
						return arith(outputArr);
				}

				return outputArr;
		}






	 	function replaceMisc(arr) {

				arr.filter(function(val, index) {


						if (arr[index] === "") {

								arr.splice(index, 1);
						}

						// negative become part of the number so not subtracting
						if (arr[index] === "-") {

								arr.splice(index, 2, "-" + arr[index + 1]);
						}


						if (arr[index] === "π") {
								arr[index] = Math.PI;
						}

						// [3, ., 5] becomes [3.5]
						if (arr[index] === ".") {

								var float = arr.splice(index - 1, 3);

								arr.splice(index - 1, 0, float.join(""));
						}



						if (arr[index] === "!") {
								// takes away number, the ! and a space that develops
								arr.splice(index - 1, 3, factorial(arr[index - 1]));
						}


						// 3(9) becomes 3x(9)
						if ((arr[index] === "(" || arr[index] === "√") && /\d/.test(arr[index - 1])) {

								arr.splice(index, 0, "x");

						}


						if (arr[index] === "(" && arr[index - 1] === ")") {
								arr.splice(index, 0, "x");

						}

						// (9)3 becomes (9)x3
						if (arr[index] === ")" && /\d/.test(arr[index + 1])) {

								arr.splice(index + 1, 0, "x");
						}

				});

				// if random space develops, delete
				if (arr[arr.length - 1] === "") {
						arr.pop();
				}
		}




		function factorial(num) {

				if (num <= 1) {
						return 1;
				}

				return num * factorial(num - 1);
		}





		 function parenthMisc(arr) {

				// confuses things when add elements to .filter so needed this instead
				while (arr.indexOf(")") !== -1) {


						var limit = arr.indexOf(")");

						// checks for nested parenthesis
						var parenth = arr.lastIndexOf("(", limit);

						var check = arr.slice(parenth + 1, limit);


						if (parenth === -1) {
								return;
						}


						var checkLen = check.length;

						check = (checkLen > 1) ? arith(check).join() : check.join();


						// deals with square roots
						if (arr[parenth - 1] === "√") {

								check = Math.sqrt(check);

								parenth--;
								checkLen++;
						}

						// if trig
						if (/N|S/.test(arr[parenth - 1])) {

								var arc = (arr[parenth - 4] === "A") ? true : false;

								// keep the parenths, will deal with in the function
								arr.splice(parenth + 1, checkLen, check);

								return trig(arr, arr[parenth - 3], arc);
						}


						// +2 for ()
						arr.splice(parenth, checkLen + 2, check);
				}

		}






		function trig(arr, letter, arc) {

				var letterInd = arr.indexOf(letter);

				var firstP = arr.indexOf("(", letterInd);
				var secondP = arr.indexOf(")", letterInd)


				var num = arr.slice(firstP + 1, secondP);

				var res = 0;


				switch (letter) {

					case "S":
							res = (arc) ? Math.asin(num) : Math.sin(num);
							break;

					case "C":
							res = (arc) ? Math.acos(num) : Math.cos(num);
							break;

					case "T":
							res = (arc) ? Math.atan(num) : Math.tan(num);
							break;

				}


				var startInd = (arc) ? letterInd - 1 : letterInd;

				// punny (varchars in sql. Not laughing? Oh)
				var chars = secondP - (startInd - 1);

				arr.splice(startInd, chars, res);
		}
